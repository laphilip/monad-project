module Throwaway where 

sumEvenNumbers :: [Int] -> Int
sumEvenNumbers = foldr (\ x acc -> if (even x) then x + acc else acc) 0