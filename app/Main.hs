module Main where

import System.Random
import Control.Monad.State

main :: IO ()
main = do
    print $ execState (playGame 100) (startState 3 OpenDoor)
    print $ execState (playGame 100) (startState 3 SwitchDoor)
    print $ execState (playGame 100) (startState 3 Randomly)

-- Monthy Hall gameState
data GameState = GameState {
    doors :: Int,       -- number of doors
    treasure :: Int,    -- door index
    wins :: Int,        -- number of wins
    losses :: Int,      -- number of losses
    strategy :: Strategy,
    generator :: StdGen
} deriving (Show)

data Strategy = OpenDoor | SwitchDoor | Randomly deriving (Eq, Show, Enum, Bounded)
instance Random Strategy where
    randomR (a, b) g = case randomR (fromEnum a, fromEnum b) g of (s, g') -> (toEnum s, g')
    random = randomR (minBound, maxBound)

startState :: Int -> Strategy -> GameState
startState numDoors strategy = GameState {
        doors = numDoors,
        treasure = index,
        wins = 0,
        losses = 0,
        strategy = strategy,
        generator = gen }
    where (index, gen) = randomR (1, numDoors) (mkStdGen 12345)

playGame :: Int -> State GameState ()
playGame 0 = return ()
playGame repetitions  = do
    currentState <- get
    selected <- selectRandomDoor
    decide selected (strategy currentState)
    playGame (repetitions -1)

selectRandomDoor :: State GameState Int
selectRandomDoor = do
    currentState <- get
    let numDoors = doors currentState
    let (selectedDoor, gen) = randomR (1, numDoors) (generator currentState)
    put currentState { generator = gen}
    return selectedDoor

decide :: Int -> Strategy -> State GameState ()
decide selected strategy = do
    currentState <- get
    let w = wins currentState
    let l = losses currentState
    let selectedTreasure = selected == treasure currentState
    case strategy of 
        OpenDoor -> if selectedTreasure 
            then put currentState {wins = w + 1} 
            else put currentState {losses = l + 1}
        SwitchDoor -> if selectedTreasure 
            then put currentState {losses = l + 1} 
            else put currentState {wins = w + 1}
        Randomly -> do
            let gen = generator currentState
            let (randomStrategy, gen') = random gen :: (Strategy, StdGen)
            put currentState { generator = gen' }
            decide selected randomStrategy
    return ()